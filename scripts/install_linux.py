#!/usr/bin/env python3

import argparse
import glob
import os
import subprocess
import sys
import ast
import time
try:
    from urllib2 import urlopen, urlencode, URLError, HTTPError, quote
except:
    from urllib.request import urlopen, URLError, HTTPError, quote
    from urllib.parse import urlencode

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])),
                             "..", "repos", "mesa_ci", "build_support"))
from utils.utils import reliable_url_open
from utils.command import run_batch_command
from project_map import ProjectMap
from export import copy

def is_excluded(a_host):
    """don't send a build to the master system, which should not automatically install a linux kernel"""
    if a_host == "master":
        return True
    if a_host == "Built-In Node":
        return True
    return False

def kernel_deb_version(path):
    (out,_) = run_batch_command(["dpkg", "-I", path], streamedOutput=False)

    return list(filter(lambda l: "Package: " in l,
                       out.decode("utf-8").
                       splitlines()))[0].replace(" Package: linux-image-","")

def install_linux(kernel_image_path, node_name):
    """On the current system, install a linux kernel at the given path"""
    local_deb_path = "/tmp/custom_kernel.deb"
    pm = ProjectMap()

    #copy kernel image via rsync and install it
    copy(kernel_image_path, local_deb_path, project_map=pm)
    run_batch_command(["dpkg", "-i", local_deb_path])
    #update grub to boot from kernel menu option in grub
    with open(f"{pm.source_root()}/scripts/custom_kernel_grub_template", "r") as ftemplate:
        with open("/etc/default/grub", "w") as fgrub:
            print(ftemplate.
                  read().
                  format(kernel_deb_version=kernel_deb_version(local_deb_path)),
                  file=fgrub)
    run_batch_command(["update-grub"])

    #trigger reboot job for host
    job_param = {'label' : node_name}
    server = "mesa-ci-jenkins.jf.intel.com"
    url = "http://" + server + "/job/public/job/reboot_single/buildWithParameters?" + urlencode(job_param)
    print("triggering " + url)
    reliable_url_open(url, method="POST")


def trigger_builds(jenkins_label, kernel_image_path, build_support_branch="origin/master", local=False):
    """On any system, determine which test machines are online and
    send a build to them"""
    server = "mesa-ci-jenkins.jf.intel.com"
    url = "http://" + server + "/computer/api/python"
    node_name = os.getenv("NODE_NAME")

    if (node_name == jenkins_label) or local:
        install_linux(kernel_image_path, node_name)
        sys.exit(0)

    host_dict = ast.literal_eval(reliable_url_open(url, method="POST").read().decode('utf-8'))
    for a_host in host_dict['computer']:
        offline = a_host['offline']
        host = a_host['displayName']
        label_list = list(map(lambda i: i["name"], a_host["assignedLabels"]))

        if is_excluded(host):
            continue
        elif jenkins_label in label_list:
            options = {'build_support_branch': build_support_branch,
                       'label': host,
                       'kernel_image_path': kernel_image_path}
            url = f"http://{server}/job/public/job/install_linux/buildWithParameters?{urlencode(options)}"
            print("triggering " + url)
            reliable_url_open(url, method="POST")
            time.sleep(1)

if __name__ == "__main__":
    argparser = argparse.ArgumentParser(description=
                                        "install a linux kernel from a (remote) path",
                                        formatter_class=argparse.RawTextHelpFormatter)
    argparser.add_argument("jenkins_label", help="can be a jenkins machine name or a label (eg. bdwgt3-01 or bdw)")
    argparser.add_argument("kernel_image_path", help="path from which to copy the kernel image (uses rsync)")
    argparser.add_argument("--build_support_branch", help="points to a sha or branch of mesa_jenkins to checkout")
    argparser.add_argument("--local", action="store_true", help="install the linux kernel on THIS machine")
    args = argparser.parse_args()

    trigger_builds(args.jenkins_label, args.kernel_image_path, build_support_branch=args.build_support_branch, local=args.local)
